# Pidgin config checks

### checks and warns the on the following:
 * if any logging is enabled
 * if connection type of account(s) are **not** set to 'Encryption Required'
 * if OTR is disabled
 * if OTR avoids logging option is disabled
 * if OTR automatic init is disabled
 * if OTR is required


### usage
#### download
`wget https://github.com/CYBERDEViLNL/pidgin_otr_check/raw/master/pidgin_otr_check.sh`

#### review the code
`cat pidgin_otr_check.sh | less`

use `q` to exit *less*

#### set execute permissions on the script
`chmod +x pidgin_otr_check.sh`

#### run the script
`./pidgin_otr_check.sh`
