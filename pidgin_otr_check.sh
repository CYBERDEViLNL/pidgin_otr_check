#!/bin/bash
# path to prefs.xml
PATH_PREFS="/home/$(whoami)/.purple/prefs.xml"

# path to accounts.xml
PATH_ACCOUNTS="/home/$(whoami)/.purple/accounts.xml"

# path to otr.private_key, only used to check if file exists
# if not it will echo a warning to attent users to generate a private key
PATH_PRIVATE_KEY="/home/$(whoami)/.purple/otr.private_key"

function cmessage {
	case $2 in
		warning)#yellow
			echo -e "$3\033[33m[WARNING] $1\033[0m"
			;;
		error)#red
			echo -e "$3\033[91m[ERROR] $1\033[0m"
			;;
		info)#italic
			echo -e "$3\033[3m[INFO] $1\033[0m"
			;;
		great)#green
			echo -e "$3\033[32m$1\033[0m"
			;;
		*)#blue
			echo -e "$3\033[34m$1\033[0m"
	esac
}
nodes=()
function get_node {
	node_count=0
	match=false
	START_NODE=$1
	END_NODE=$2
	CONTENT=$3
	while read -r line; do
		if [ "$line" == "$START_NODE" ]; then
			match=true
		fi
		if $match; then
			nodes[$node_count]="${nodes[$node_count]}$line"
		fi
		if [ "$line" == "$END_NODE" ]; then
			let ++node_count
			match=false
		fi
	done <<< "$CONTENT"
}

cmessage "Pidgin check"
echo "This script will try to read the following files and do some checks:"
echo -e "\t $PATH_PREFS"
echo -e "\t $PATH_ACCOUNTS"
echo -e "\t $PATH_PRIVATE_KEY\n"

echo -n "Do you want to continue: [yes/no] "
read _CHECK

# To support all cases of Yes
_CHECK=$(echo $_CHECK | tr '[:upper:]' '[:lower:]')
if [ "$_CHECK" = "yes" ]; then
	if ! [ -e "$PATH_PREFS" ]; then
		cmessage "$PATH_PREFS does not exist. Exit." "error"
		exit
	fi
	if ! [ -r "$PATH_PREFS" ]; then
		cmessage "Do not have permission to read $PATH_PREFS. Exit." "error"
		exit
	fi

	## prefs.xml
	function read_prefs {
		echo $(sed -n "s/\s*<pref name='$1' type='bool' value='\([01]\)'\/>/\1/pI" $PATH_PREFS)
	}
	LOG_IMS=$(read_prefs "log_ims")
	LOG_CHATS=$(read_prefs "log_chats")
	LOG_SYSTEM=$(read_prefs "log_system")

	function process_prefs {
		if [ $2 != 0 ]; then
			LOG_WARNING=1
			case $1 in
				log_ims)
					cmessage "All instant messages are logged!" "warning" "\t"
					;;
				log_chats)
					cmessage "All chats are logged!" "warning" "\t"
					;;
				log_system)
					cmessage "All status changes are logged to system!" "warning" "\t"
					;;
			esac
		fi
	}
	process_prefs "log_ims" $LOG_IMS
	process_prefs "log_chats" $LOG_CHATS
	process_prefs "log_system" $LOG_SYSTEM

	if [ $LOG_WARNING ]; then
		cmessage """You can find the log settings in Pidgin's top menu:
		Tools -> preferences -> [TAB] Logging
""" "info" "\t\t"
	fi

	get_node "<pref name='loaded' type='pathlist'>" "</pref>" "$(cat $PATH_PREFS)"
	if [ "$(echo ${nodes[@]} | grep -o 'pidgin-otr.so')" != "pidgin-otr.so" ]; then
		cmessage "OTR plugin is not loaded." "warning" "\t"
		cmessage """To load the OTR (Off-the-Record Messaging) plugin, go to
		Pidgin's top menu: 'Tools' -> 'Plugins'
		a new window wil appear. Find the 'Off-the-Record Messaging' plugin and
		check the box in front of it.
		""" "info" "\t\t"
		cmessage """If this is the first time you loaded there are no private keys yet.
		To generate private keys click on the 'Off-the-Record Messaging' plugin
		followed by the 'Configure Plugin' button.
		a new window wil appear
		Select a account from the list and press 'Generate'
		""" "info" "\t\t"
		cmessage """You also want to make sure the following checkboxes are checked:
		[x] Enable private messaging
		[x] Automatically initiate private messaging
		[x] Require private messaging
		[x] Don't log OTR conversations
		""" "info" "\t\t"
	fi

	if ! [ -e "$PATH_PRIVATE_KEY" ]; then
		cmessage "No OTR private keys found, please generate some." "warning" "\t"
	fi

	OTR_SETTINGS=$(grep -Poz "(?s)<pref name='OTR'>.*/>\s+</pref>" $PATH_PREFS | tr '\0' '\n')
	function read_accounts {
		echo $(echo $OTR_SETTINGS | sed -n "s/.*<pref name='$1' type='bool' value='\([01]\)'\/>.*/\1/pI")
	}
	if [ "$OTR_SETTINGS" ]; then
		OTR_ENABLED=$(read_accounts "enabled")
		OTR_AUTOMATIC=$(read_accounts "automatic")
		OTR_ONLY_PRIVATE=$(read_accounts "onlyprivate")
		OTR_AVOID_LOGGING=$(read_accounts "avoidloggingotr")
		function process_otr_settings {
			if [ $2 != 1 ]; then
				OTR_WARNING=1
				case $1 in
					enabled)
						cmessage "OTR is disabled." "warning" "\t"
						;;
					automatic)
						cmessage "OTR automatic is disabled. Private messaging will not automaticly initiate!" "warning" "\t"
						;;
					onlyprivate)
						cmessage "Require OTR is disabled." "warning" "\t"
						;;
					avoidloggingotr)
						cmessage "OTR avoid logging option is disabled." "warning" "\t"
						;;
				esac
			fi
		}
		process_otr_settings "enabled" $OTR_ENABLED
		process_otr_settings "automatic" $OTR_AUTOMATIC
		process_otr_settings "onlyprivate" $OTR_ONLY_PRIVATE
		process_otr_settings "avoidloggingotr" $OTR_AVOID_LOGGING
		if [ $OTR_WARNING ]; then
			cmessage """You can find the OTR settings in Pidgin's top menu:
		'Tools' -> 'Plugins'
		then select 'Off-the-Record Messaging' and press 'Configure Plugin'
""" "info" "\t\t"
		fi

	else
		cmessage "Could not read OTR settings, do you have pidgin-otr installed AND enabled?" "error" "\t"
	fi

	## accounts.xml
	if ! [ -e "$PATH_ACCOUNTS" ]; then
		cmessage "$PATH_ACCOUNTS does not exist. Exit." "error"
		exit
	fi
	if ! [ -r "$PATH_ACCOUNTS" ]; then
		cmessage "Do not have permission to read $PATH_ACCOUNTS. Exit." "error"
		exit
	fi

	get_node "<account>" "</account>" "$(cat $PATH_ACCOUNTS)"
	accounts=("${nodes[@]}")

	for account in "${accounts[@]}"; do
		ACCOUNT_NAME=$(echo $account | grep -o "<name>.*</name>" | sed -n "s/<name>\(.*\)<\/name>/\1/pI")
		ACCOUNT_PRPL=$(echo $account | grep -o "<protocol>.*</protocol>" | sed -n "s/<protocol>\(.*\)<\/protocol>/\1/pI")

		CONNECTION_SECURITY=$(echo $account | grep -Po "<setting name='connection_security' type='string'>.*?</setting>" | sed -n "s/<setting name='connection_security' type='string'>\(.*\)<\/setting>/\1/pI")
		ENCRYPTION=$(echo $account | grep -Po "<setting name='encryption' type='string'>.*?</setting>" | sed -n "s/<setting name='encryption' type='string'>\(.*\)<\/setting>/\1/pI")

		if [ $CONNECTION_SECURITY ] && [ "$CONNECTION_SECURITY" != "require_tls" ]; then
			cmessage "$ACCOUNT_PRPL account '$ACCOUNT_NAME' is not configured to require encryption." "warning" "\t"
			ENCRYPTION_WARNING=1
		fi
		if [ $ENCRYPTION ] && [ "$ENCRYPTION" != "require_encryption" ]; then
			cmessage "$ACCOUNT_PRPL account '$ACCOUNT_NAME' is not configured to require encryption." "warning" "\t"
			ENCRYPTION_WARNING=1
		fi
	done

	if [ $ENCRYPTION_WARNING ]; then
		cmessage """There is no encrypted connection between you and the server.
		To set the encryption option to 'Required Encryption' go to Pidgin's top menu ->
		'Accounts' -> '<the account of intrest>' -> 'Edit Account'
		a new window wil open, go to the 'Advanced' tab and set the 'Connection security'
		to 'Encryption Required'
""" "info" "\t\t"
	fi

	if [ $ENCRYPTION_WARNING ] || [ $OTR_WARNING ] || [ $LOG_WARNING ]; then
		cmessage "Please fix the above.\n"
	else
		cmessage "Great everything looks fine.\n" "great"
	fi
fi
